let moveSize = 30
let jumpSize = -40
let container = document.getElementById('container')
let p1HP = document.getElementById('player1HP')
let p2HP = document.getElementById('player2HP')

class Element {
    constructor(name, elem) {
        this.name = name;
        this.elem = document.createElement('div');
        this.elem.id = elem;
        container.appendChild(this.elem);
        this.x = getPlayerPosition(this.elem)[1];
        this.y = getPlayerPosition(this.elem)[0];
    }
    update() {
        this.x = getPlayerPosition(this.elem)[1];
        this.y = getPlayerPosition(this.elem)[0];
    }
}

class Fighter extends Element {
    constructor(name, elem, hpelem, hpcontainer) {
        super(name, elem);
        this.hp = 100;
        this.status = 'normal';
        this.hpcontainer = hpcontainer;
        this.hpelem = document.createElement('div');
        this.hpelem.id = hpelem;
        this.hpcontainer.appendChild(this.hpelem);
        this.flipKick = function (fighter) {
            let attackRange = 90;
            let fighterDistance = Math.abs(fighter.x - this.x);
            //animation
            let pieceRotation = [
                { transform: 'rotate(0)' },
                { transform: 'rotate(360deg)' }
            ];
            let animationOptions = {
                duration: 500
            };
            this.elem.animate(pieceRotation, animationOptions);
            //logic
            let damage = 10;
            if (fighterDistance <= attackRange) {
                if (fighter.status === 'weakened') {
                    damage *= 2;
                    fighter.hp -= damage;
                }
                else {
                    fighter.hp -= damage;
                }
                let hp = window.getComputedStyle(fighter.hpelem);
                hp = parseInt(hp.getPropertyValue('width'));
                if ((hp - damage * 2) >= 0) {
                    fighter.hpelem.style.width = (hp - damage * 2) + 'px';
                }
                else if ((hp - damage * 2) < 0) {
                    fighter.hpelem.style.width = 0 + 'px';
                }
            }
        };
        this.teleportAttack = function (fighter) {
            let attackRange = 30;
            let fighterDistance = Math.abs(fighter.x - this.x);
            //animation
            let pieceRotation = [
                { transform: 'scaleY(-1)' },
                { transform: 'scaleY(1)' }
            ];
            let animationOptions = {
                duration: 500
            };
            this.elem.animate(pieceRotation, animationOptions);
            //logic
            let damage = 20;
            if (fighterDistance <= attackRange) {
                if (fighter.status === 'weakened') {
                    damage *= 2;
                    fighter.hp -= damage;
                }
                else {
                    fighter.hp -= damage;
                }
                let hp = window.getComputedStyle(fighter.hpelem);
                hp = parseInt(hp.getPropertyValue('width'));
                if ((hp - damage * 2) >= 0) {
                    fighter.hpelem.style.width = (hp - damage * 2) + 'px';
                }
                else if ((hp - damage * 2) < 0) {
                    fighter.hpelem.style.width = 0 + 'px';
                }
            }
        };
        this.weaken = function (fighter) {
            let attackRange = 80;
            let fighterDistance = Math.abs(fighter.x - this.x);
            if (fighterDistance <= attackRange) {
                if (fighter.status === 'normal') {
                    fighter.status = 'weakened';
                    fighter.elem.style.border = 1 + 'px solid red';
                    fighter.elem.style.borderRadius = 10 + 'px';
                    setTimeout(function () {
                        fighter.status = 'normal';
                        fighter.elem.style.border = 'none';
                    }, 3000);
                }
            }
            let pieceRotation = [
                { transform: 'skewY(45deg)' },
                { transform: 'skewY(0deg)' }
            ];
            let animationOptions = {
                duration: 500
            };
            this.elem.animate(pieceRotation, animationOptions);
        };
    }
}

let scorpion = new Fighter('Scorpion', 'char1', 'player1HPinner', p1HP)
let subZero = new Fighter('Sub-Zero','char2', 'player2HPinner', p2HP)

document.addEventListener('keydown', function keyHandler(){
    scorpion.update()
    subZero.update()
    
    // subzero controls
    if(event.key === "ArrowRight"){
        subZero.x += moveSize
        subZero.elem.style.left = subZero.x + 'px'
    } else if(event.key === "ArrowLeft") {
        subZero.x -= moveSize
        subZero.elem.style.left = subZero.x + 'px'
    } else if(event.key === 'ArrowUp') {
        subZero.y += jumpSize
        subZero.elem.style.top = subZero.y + 'px'
        setTimeout(function(){
            subZero.y -= jumpSize
            subZero.elem.style.top = subZero.y + 'px'
        }, 500)
    } else if (event.key === 'ArrowDown') {
        subZero.flipKick(scorpion)
    } else if(event.key === '/') {
        subZero.teleportAttack(scorpion)
    } else if(event.key === '.') {
        subZero.weaken(scorpion)
    }

    // scorpion controls
    if(event.key === "a"){
        scorpion.x -= moveSize
        scorpion.elem.style.left = scorpion.x + 'px'
    } else if(event.key === "d") {
        scorpion.x += moveSize
        scorpion.elem.style.left = scorpion.x + 'px'
    } else if(event.key === 'w') {
        scorpion.y += jumpSize
        scorpion.elem.style.top = scorpion.y + 'px'
        setTimeout(function(){
            scorpion.y -= jumpSize
            scorpion.elem.style.top = scorpion.y + 'px'
        }, 500)
    } else if(event.key === 's'){
        scorpion.flipKick(subZero)
    } else if(event.key === 'q') {
        scorpion.teleportAttack(subZero)
    } else if(event.key === 'e') {
        scorpion.weaken(subZero)
    }

    if(scorpion.hp <= 0) {
        let winDiv = document.createElement('div')
        winDiv.id = 'winner'
        let winText = document.createTextNode('Sub-Zero wins')
        winDiv.appendChild(winText)
        document.body.appendChild(winDiv)
        document.removeEventListener('keydown', keyHandler)
    } else if (subZero.hp <= 0) {
        let winDiv = document.createElement('div')
        winDiv.id = 'winner'
        let winText = document.createTextNode('Scorpion wins')
        winDiv.appendChild(winText)
        document.body.appendChild(winDiv)
        document.removeEventListener('keydown', keyHandler)
    }
})

function getPlayerPosition(div) {
    let playerStyle = window.getComputedStyle(div)
    let playerBot = parseInt(playerStyle.getPropertyValue('top'))
    let playerLeft = parseInt(playerStyle.getPropertyValue('left'))
    return [playerBot, playerLeft]
}